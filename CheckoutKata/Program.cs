﻿using CheckoutKata.Models;
using System;

namespace CheckoutKata
{
    class Program
    {
        static void Main(string[] args)
        {
            var order = new Order();

            try
            {
                var item = new Item() { ProductCode = "Apples", Count = 1 };
                order.Scan(item);

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                order.Scan(item);

                Bill(order);

                item = new Item() { ProductCode = "Oranges", Count = 3 };
                order.Scan(item);

                Bill(order);

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                order.Scan(item);

                Bill(order);

                item = new Item() { ProductCode = "Bananas", Count = 1 };
                order.Scan(item);

                item = new Item() { ProductCode = "Oranges", Count = 1 };
                order.Scan(item);
                Bill(order);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void Bill(Order order)
        {
            foreach (var element in order.Items)
            {
                Console.WriteLine($" {element.ProductCode} - {element.UnitPrice} - {element.Count} - {element.Amount}");
            }
            Console.WriteLine($"Total {order.Total()}");
            Console.WriteLine();
        }

    }
}
