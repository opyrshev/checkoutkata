﻿using CheckoutKata.Data;
using CheckoutKata.Engine;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CheckoutKata.Models
{
    public class Order : ICheckout
    {
        public List<Item> Items { get; set; } = new();

        public void Scan(Item item)
        {
            var price = Goods.GetPrice().Where(x => x.ProductCode == item.ProductCode).FirstOrDefault();

            if (price == null)
            {
                throw new Exception($"There is no {item.ProductCode} in the Price");
            }

            var count = Items.Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Count) + item.Count;
            Items.RemoveAll(x => x.ProductCode == item.ProductCode);

            if (price.SpecialOfferCount != null)
            {
                var usualCount = (count % price.SpecialOfferCount)?? count;
                AddItem(price.ProductCode, usualCount, usualCount * price.UnitPrice);

                var specialCount = count - usualCount;
                AddItem(price.ProductCode, specialCount, specialCount / price.SpecialOfferCount * price.SpecialOfferPrice?? 0);
            }
            else
            {
                item.Count = count;
                item.UnitPrice = price.UnitPrice;
                item.Amount = item.UnitPrice * item.Count;
                Items.Add(item);
            }
        }

        public double Total()
        {
            return Items.Sum(x => x.Amount)?? 0;
        }

        private void AddItem(string productCode, int count, double amount)
        {
            if (count == 0)
            {
                return;
            }

            var item = new Item()
            { 
                ProductCode = productCode,
                Count = count,
                Amount = amount,
                UnitPrice = amount / count
            };

            Items.Add(item);
        }
    }
}
