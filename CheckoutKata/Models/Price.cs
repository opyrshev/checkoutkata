﻿namespace CheckoutKata.Models
{
    public class Price
    {
        public string ProductCode { get; set; }

        public double UnitPrice { get; set; }

        public int? SpecialOfferCount { get; set; }

        public double? SpecialOfferPrice { get; set; }
    }
}
