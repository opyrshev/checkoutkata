﻿using CheckoutKata.Models;

namespace CheckoutKata.Engine
{
    public interface ICheckout
    {
        void Scan(Item item);
        double Total();

    }
}
