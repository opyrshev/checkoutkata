﻿using System;
using System.Collections.Generic;
using System.Linq;
using CheckoutKata.Data;
using CheckoutKata.Models;
using NUnit.Framework;
using static System.Math;

namespace CheckoutKata.Test
{
    [TestFixture]
    public class OrgerTest
    {
        private Order _sut;
        private List<Price> _price;

        [SetUp]
        public void SetUp()
        {
            _sut = new Order();
            _price = Goods.GetPrice();
        }

        [Test]
        public void Scan_Without_Special_Offer_Test()
        {
            var item = new Item() { ProductCode = "Apples", Count = 3 };
            var expectedAmount = _price.Where(x => x.ProductCode == item.ProductCode).Sum(x => x.UnitPrice) * item.Count;
            _sut.Scan(item);
            Assert.AreEqual(_sut.Items.Count(x => x.ProductCode == item.ProductCode), 1);
            Assert.AreEqual(_sut.Items.Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Count), item.Count);
            Assert.AreEqual(_sut.Items.Where(x => x.ProductCode == item.ProductCode).Sum(x => x.Amount), expectedAmount);
        }

        [Test]
        public void Scan_With_Special_Offer_Test()
        {
            var item1 = new Item() { ProductCode = "Oranges", Count = 3 };
            var expectedAmount1 = Round((_price.Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.SpecialOfferPrice / x.SpecialOfferCount) * item1.Count) ?? 0, 2);

            _sut.Scan(item1);
            var resultAmount1 = Round(_sut.Items.Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.Amount) ?? 0, 2);
            Assert.AreEqual(_sut.Items.Count(x => x.ProductCode == item1.ProductCode), 1);
            Assert.AreEqual(_sut.Items.Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.Count), item1.Count);
            Assert.AreEqual(resultAmount1, expectedAmount1);

            var item2 = new Item() { ProductCode = "Oranges", Count = 2 };
            var expectedAmount2 = Round((_price.Where(x => x.ProductCode == item1.ProductCode).Sum(x => x.UnitPrice) * item2.Count), 2);

            _sut.Scan(item2);
            var resultAmount2 = Round(_sut.Items.Where(x => x.ProductCode == item2.ProductCode).Sum(x => x.Amount) ?? 0, 2);
            Assert.AreEqual(_sut.Items.Count(x => x.ProductCode == item2.ProductCode), 2);
            Assert.AreEqual(_sut.Items.Where(x => x.ProductCode == item2.ProductCode).Sum(x => x.Count), item2.Count + item1.Count);
            Assert.AreEqual(resultAmount2, expectedAmount2 + expectedAmount1);
        }

        [Test]
        public void Total_Test()
        {
            var item1 = new Item() { ProductCode = "Oranges", Count = 3 };
            _sut.Scan(item1);
            var item2 = new Item() { ProductCode = "Apples", Count = 2 };
            _sut.Scan(item2);
            var expectedResult = Round(_sut.Items.Sum(x => x.Amount)?? 0, 2);
            var result = Round(_sut.Total(), 2);
            Assert.AreEqual(result, expectedResult);
        }

        [Test]
        public void Scan_With_Incorrect_Item_Test()
        {
            var item = new Item() { ProductCode = "Apple", Count = 3 };

            var ex = Assert.Throws<Exception>(() => _sut.Scan(item));
            Assert.AreEqual($"There is no {item.ProductCode} in the Price", ex.Message);
        }
    }
}
